from datetime import datetime
from flask import Flask, render_template, url_for, flash, redirect
from flask_sqlalchemy import SQLAlchemy
from whitenoise import WhiteNoise

app = Flask(__name__)
app.wsgi_app = WhiteNoise(app.wsgi_app, root='static/')

@app.route('/hello')
def hello():
    return 'Hello, Worlds!'

@app.route('/guideline')
def guideline():
    return 'This will be a guideline'

@app.route('/')
@app.route('/home')
def home():
    return render_template('the_eccg.html', title = 'Home')


@app.route('/about')
def about():
    return render_template('about.html', title = 'About')

@app.route('/the_eccg')
def the_eccg():
    return render_template('the_eccg.html', title = 'The Edge to Cloud Code Generator')

#so we can run as script
if __name__ == "__main__":
    app.run(debug=True)
