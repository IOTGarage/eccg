# Edge to Cloud Generator (ECCG)
#### Please read the following statements before using the Edge to Cloud Generators.
- The code generator designed to be work with Grove sensors.
- You need to select at least one sensor type and communication method while also entering sleeping time to generate working code.
- The generated code is a minimal working example unless you select all sensors and should be edited according to your needs.
- The generated code includes debugging part to let you debug your code via serial monitor if you encounter any issues.
- The grove modules do not let you to use custom libraries such as "Esp.h".
- The AT commands are used to configure communication modules, please check your module's documentation to see available commands.
- The code generator utilize modified  [Grove example codes](https://www.seeedstudio.com/category/Grove-c-1003.html).
- Current version assumes Arduino Leonardo is used. Serial1 should be changed into Software Serial for other microcontroller types.
- If BLE is selected the generated code assumes you are using ArduinoBLE.h compatible device.
- Additionally you might require download related Grove libraries.
- Please let us know if you have any <a  href="mailto:kayanh@cardiff.ac.uk">inquiries.
- Link to the application: https://eccg.herokuapp.com/.
